#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  query = region_actual_size( size_from_capacity((block_capacity) {.bytes = query}).bytes);
  struct region rg = {0};
  void* mem_alloc = map_pages(addr, query, MAP_FIXED_NOREPLACE); 
  
  if (mem_alloc == MAP_FAILED) {
      mem_alloc = map_pages(addr, query, 0); 
  }
 
  if (mem_alloc == MAP_FAILED) {
    return REGION_INVALID;
  } else { 
    rg.addr = mem_alloc;
  }

  rg.size = query;
  rg.extends = (addr == mem_alloc);
  block_size a = {.bytes = query};
  block_init(mem_alloc,  a, NULL);
  
  return rg;
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}


#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  size_t new_block1_size = (query < BLOCK_MIN_CAPACITY) ? BLOCK_MIN_CAPACITY : query;
  if (!block || !block_splittable(block, query)) return false;

  size_t remaining_size = block->capacity.bytes - new_block1_size;
  struct block_header* new_block2_addr = (struct block_header*)(block->contents + new_block1_size);

  block_init(new_block2_addr, (block_size){remaining_size}, block->next);
  block_init(block, size_from_capacity((block_capacity){new_block1_size}), new_block2_addr);

  return true;
}



/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block) {
  if (block != NULL) {
    struct block_header* next_block = block->next;
    if ((next_block != NULL) && (mergeable(block, next_block))) {
        block->next = next_block->next;
        block->capacity.bytes = block->capacity.bytes + size_from_capacity(next_block->capacity).bytes;
        return true;
    }
  }
  return false;
}

void heap_term() {
    struct block_header* start_region = HEAP_START;
    while (start_region) {
        size_t l = 0;
        struct block_header* start_reg = start_region;
        while (start_region->next == block_after(start_region)) {
            l += size_from_capacity(start_region->capacity).bytes;
            start_region = start_region -> next;
        }
        l += size_from_capacity(start_region->capacity).bytes;
        start_region = start_region->next;
        munmap(start_reg, l);
    }
}

/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last ( struct block_header* restrict block, size_t sz )    {
  struct block_header* cur = block;

  if (cur == NULL) {
    return (struct block_search_result) {.block = cur, .type = BSR_CORRUPTED};
  }

  while (cur) {
    while (try_merge_with_next(cur));
    if (cur->is_free && block_is_big_enough(sz, cur)) {
      return (struct block_search_result) {.block=cur, .type = BSR_FOUND_GOOD_BLOCK};
    } else {
      if (!try_merge_with_next(cur)) {
        if(cur->next == NULL) break;
        cur = cur->next;
      }
    }
  }
  return (struct block_search_result) {.block=cur, .type = BSR_REACHED_END_NOT_FOUND};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result res = find_good_or_last(block, query);
  if(res.type == BSR_FOUND_GOOD_BLOCK) {
      split_if_too_big(res.block, query);
      res.block->is_free = false;
  }
  return res;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  if(last == NULL) {
    return NULL;
  }
  struct region reg = alloc_region(last->contents + last->capacity.bytes, query);
  if(reg.addr == NULL) {
    return NULL;
  }
  if(last->is_free && reg.extends) {
    last->capacity.bytes = last->capacity.bytes + reg.size;
    return last;
  }
  last->next = reg.addr;
 
  return reg.addr;

}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  query = size_max(query, BLOCK_MIN_CAPACITY);
  struct block_search_result bl = try_memalloc_existing(query, heap_start);
  
  
  if(bl.type == BSR_FOUND_GOOD_BLOCK) {
    return bl.block;
  }
  struct block_header* gh = grow_heap(bl.block, query);

  if(gh != NULL) {
    return try_memalloc_existing(query, heap_start).block;
  }

  return NULL;

}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while(try_merge_with_next(header));
}
